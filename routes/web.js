var adminController = require('../apps/controllers/admin');
var auth = require('../apps/middlewares/auth');

module.exports = function(app){
	
	app.group("/admin", (router) => {
		
		router.use(function(req, res, next){
			
			return auth.guest(req, res, next);
		});
		
		router.get('/dashboard', adminController.dashboard);
		router.get('/product/list', adminController.productList);
		
		router.get('/product/add', adminController.productGetAdd);
		router.post('/product/add', adminController.productPostAdd);
		
		router.get('/product/edit/:prd_id', adminController.productGetEdit);
		router.post('/product/edit/:prd_id', adminController.productPostEdit);
		
		router.get('/product/del/:prd_id', adminController.productDel);
		
		router.get('/logout', adminController.logout);
		
		router.get('/test1', adminController.test1);
		router.get('/test2', adminController.test2);
	});
	
	app.group('/login', (router) => {
		
		router.use(function(req, res, next){
			return auth.check(req, res, next);
		});
		
		router.get('/', adminController.getLogin);
		router.post('/', adminController.postLogin);
	});
}




